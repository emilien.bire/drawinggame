using System.Text;
using UnityEngine;

public static class Tools
{
    public static float[,] Convolution(float[,] array, float[,] kernel)
    {
        float[,] convolved = new float[array.GetLength(0), array.GetLength(1)];
        int imageHeight = array.GetLength(0);
        int imageWidth = array.GetLength(1);
        int w = kernel.GetLength(0);
        int center = (int)w/2;

        for (int y = 0; y < imageHeight; y++)
        {
            for (int x = 0; x < imageWidth; x++)
            {
                float sum = 0;
                for (int i = -center; i <= center; i++)
                {
                    for (int j = -center; j <= center; j++)
                    {
                        //sum += kernel[center - j, center - i] * (1 - array[x - j, y - i]);
                        sum += ((y - j) >= 0 && (x - i) >= 0 && (y - j) < imageHeight && (x - i) < imageWidth) ? kernel[center - i,center - j] * (1-array[y - j, x - i]) : kernel[center - i, center - j] * (1-array[y,x]);
                        //Debug.Log(((x - j) >= 0 && (y - i) >= 0 && (x - j) < imageWidth && (y - i) < imageHeight) ? kernel[center - i, center - j] * (1 - array[x - j, y - i]) : 0);
                    }
                }
                convolved[y, x] = Mathf.Clamp(1-sum,0f,1f);
            }
        }
        //Print2DArray(convolved);
        return convolved;
    }

    public static float[,] ArrayTo2DArray(float[] list, int sizeX, int sizeY)
    {
        float[,] array = new float[sizeY, sizeX];
        for (int y = 0; y < sizeY; y++)
        {
            for (int x = 0; x < sizeX; x++)
            {
                array[y, x] = list[x*sizeY + y];
            }
        }
        return array;
    }

    public static float[] flatten(float[,] array)
    {
        float[] list = new float[array.GetLength(0) * array.GetLength(1)];
        for (int x = 0; x < array.GetLength(0); x++)
        {
            for (int y = 0; y < array.GetLength(1); y++)
            {
                list[x * array.GetLength(0) + y] = array[y, x];
            }
        }
        return list;
    }

    public static float[,] BlurKernel(float sigma, int size)
    {
        float[,] kernel = new float[2*size+1, 2*size+1];
        int center = size;
        kernel[center,center] = (1 / (Mathf.PI * 2 * sigma * sigma));
        for (int i = 1; i <= center; i++)
        {
            kernel[center - i, center] = (1 / (Mathf.PI * 2 * sigma * sigma)) * Mathf.Exp(-(i * i) / (2 * sigma * sigma));
            kernel[center + i, center] = (1 / (Mathf.PI * 2 * sigma * sigma)) * Mathf.Exp(-(i * i) / (2 * sigma * sigma));
            kernel[center, center + i] = (1 / (Mathf.PI * 2 * sigma * sigma)) * Mathf.Exp(-(i * i) / (2 * sigma * sigma));
            kernel[center, center - i] = (1 / (Mathf.PI * 2 * sigma * sigma)) * Mathf.Exp(-(i * i) / (2 * sigma * sigma));
            
            for (int j = 1; j <= center; j++)
            {
                kernel[center - i, center - j] = (1 / (Mathf.PI * 2 * sigma * sigma)) * Mathf.Exp(-(i * i + j * j) / (2 * sigma * sigma));
                kernel[center + i, center - j] = (1 / (Mathf.PI * 2 * sigma * sigma)) * Mathf.Exp(-(i * i + j * j) / (2 * sigma * sigma));
                kernel[center - i, center + j] = (1 / (Mathf.PI * 2 * sigma * sigma)) * Mathf.Exp(-(i * i + j * j) / (2 * sigma * sigma));
                kernel[center + i, center + j] = (1 / (Mathf.PI * 2 * sigma * sigma)) * Mathf.Exp(-(i * i + j * j) / (2 * sigma * sigma));

            }
        }

        return kernel;
    }


    public static float[,] SharpeningFilter(int strength)
    {
        float edgeFactor = 1;
        float[,] kernel = new float[3, 3]
            {{edgeFactor,-2*edgeFactor,edgeFactor},
            {-2*edgeFactor,strength+1,-2*edgeFactor},
            {edgeFactor,-2*edgeFactor,edgeFactor}};
        return kernel;
    }

    public static Color[] GreyScaleToRGB(float[] gsColors)
    {
        Color[] RGBColors = new Color[gsColors.Length];
        for (int i = 0; i < gsColors.Length; i++)
        {
            RGBColors[i] = new Color(gsColors[i], gsColors[i], gsColors[i]);
        }

        return RGBColors;
    }

    public static void Print2DArray(float[,] data)
    {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < data.GetLength(1); i++)
        {
            for (int j = 0; j < data.GetLength(0); j++)
            {
                sb.Append(data[i, j]);
                sb.Append(' ');
            }
            sb.AppendLine();
        }
        Debug.Log(sb.ToString());
    }

    public static void Print1DArray(float[] data)
    {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < data.Length; i++)
        {
            sb.Append(data[i]);
            sb.Append(' ');
            sb.AppendLine();
        }
        Debug.Log(sb.ToString());
    }

    public static Color AddColors(Color color1, Color color2)
    {
        Color addition = new Color(Mathf.Clamp(color1.r + color2.r, 0f, 1f),
                                   Mathf.Clamp(color1.g + color2.g, 0f, 1f),
                                   Mathf.Clamp(color1.b + color2.b, 0f, 1f));
        return addition;
    }

    public static float[,] ConvertTo255(float[,] data)
    {
        int w = data.GetLength(0);
        int h = data.GetLength(1);
        float[,] modifiedData = new float[w, h];
        for (int i = 0; (i < w);i++)
        {
            for(int j = 0; j < h; j++)
            {
                modifiedData[i,j] = data[i, j] * 255;
            }
        }
        return modifiedData;
    }

    public static float[,] ConvertTo01(float[,] data)
    {
        int w = data.GetLength(0);
        int h = data.GetLength(1);
        float[,] modifiedData = new float[w, h];
        for (int i = 0; (i < w); i++)
        {
            for (int j = 0; j < h; j++)
            {
                modifiedData[i, j] = data[i, j] / 255;
            }
        }
        return modifiedData;
    }
    public static float map(float s, float a1, float a2, float b1, float b2)
    {
        return b1 + (s - a1) * (b2 - b1) / (a2 - a1);
    }

}
