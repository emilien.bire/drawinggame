using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using Unity.Mathematics;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

public class DrawerScript : MonoBehaviour
{
    public Camera cam;
    public Slider brushSizeSlider;
    public TMP_Text brushSizeText;

    public TMP_Dropdown brushDropdown;

    public Slider colorSlider;

    public Slider sigmaSlider;
    public TMP_Text sigmaText;

    public Slider colorationSpeedSlider;
    public TMP_Text colorationSpeedText;

    public string brushType;
    public float minBrushSize = 2;
    public float maxBrushSize = 20;
    private float brushSize;
    public Color brushColor;
    public float coloredSpeed;
    public float blurBrushForce;

    private void Start()
    {
        string[] brushTypesString = Enum.GetNames(typeof(brushTypes));
        brushDropdown.AddOptions(brushTypesString.ToList());
    }

    void Update()
    {
        brushSize = brushSizeSlider.value;
        brushSizeText.SetText(brushSize.ToString());

        blurBrushForce = sigmaSlider.value;
        sigmaText.SetText(blurBrushForce.ToString());

        coloredSpeed = colorationSpeedSlider.value;
        colorationSpeedText.SetText(coloredSpeed.ToString());

        float bwCol = colorSlider.value;
        brushColor = new Color(bwCol, bwCol, bwCol);

        if (Input.GetMouseButton(0))
        {
            Draw2();
        }

        
    }
    public void Draw()
    {

        RaycastHit hit;
        if (!Physics.Raycast(cam.ScreenPointToRay(Input.mousePosition), out hit))
        {
            return;
        }
        
        MeshCollider meshCollider = hit.collider as MeshCollider;
        if (meshCollider == null || meshCollider.sharedMesh == null)
        {
            return;
        }
        Mesh mesh = meshCollider.sharedMesh;
        Vector3[] vertices = mesh.vertices;
        int[] triangles = mesh.triangles;
        int triInd = hit.triangleIndex;
        

        Color[] colors = mesh.colors;
        Color color = brushColor;

        List<int> triangleIndexes = new List<int>();
        triangleIndexes.Add(triInd);

        int numbVertPerSide = MeshGenerator.instance.zSize;
        for (int i = 1; i < brushSize; i++)
        {
            triangleIndexes.Add(triInd + 2 * (i));
            triangleIndexes.Add(triInd + 2 * (- numbVertPerSide * i));
            triangleIndexes.Add(triInd + 2 * (-i));
            triangleIndexes.Add(triInd + 2 * (numbVertPerSide * i));
            for (int j=1; j < brushSize; j++)
            {
                if (Mathf.Sqrt(i*i+j*j)<=brushSize)
                {
                    triangleIndexes.Add(triInd + 2 * (i + numbVertPerSide * j));
                    triangleIndexes.Add(triInd + 2 * (i - numbVertPerSide * j));
                    triangleIndexes.Add(triInd + 2 * (-i - numbVertPerSide * j));
                    triangleIndexes.Add(triInd + 2 * (-i +  numbVertPerSide * j));
                }
            }
        }

        List<int> modifiedTrianglesIndices = new List<int>();
        for (int i = 0; i < triangleIndexes.Count; i++)
        {
            int triangleIndex = triangleIndexes[i];
            
            for (int j = 0; j < 3; j++)
            {
                int ind1 = Mathf.Clamp(triangleIndex * 3 + j, 0, triangles.Length - 1);
                modifiedTrianglesIndices.Add(triangles[ind1]);
                print(triangles[ind1]);
                if (triangleIndex % 2 == 0)
                {
                    int ind2 = Mathf.Clamp((triangleIndex + 1) * 3 + j, 0, triangles.Length - 1);
                    modifiedTrianglesIndices.Add(triangles[ind2]);
                    print(triangles[ind2]);
                }
                else
                {
                    int ind2 = Mathf.Clamp((triangleIndex - 1) * 3 + j, 0, triangles.Length - 1);
                    print(triangles[ind2]);
                    modifiedTrianglesIndices.Add(triangles[ind2]);
                }
            }
        }

        brushTypes bt = (brushTypes)brushDropdown.value;
        brushType = bt.ToString();

        foreach (int modifTrInd in modifiedTrianglesIndices)
        {
            if (brushType == "Solid")
            {
                Color newCol = color;
                colors[modifTrInd] = newCol;
            }
            if (brushType == "SolidAdditive")
            {
                Color addNewCol = new Color(-coloredSpeed * Time.deltaTime, -coloredSpeed * Time.deltaTime, -coloredSpeed * Time.deltaTime);
                Color initColor1 = colors[modifTrInd];
                Color newCol1 = Tools.AddColors(initColor1, addNewCol);
                colors[modifTrInd] = newCol1;
            }
            if (brushType == "SolidSoustractive")
            {
                Color addNewCol = new Color(coloredSpeed * Time.deltaTime, coloredSpeed * Time.deltaTime, coloredSpeed * Time.deltaTime);
                Color initColor1 = colors[modifTrInd];
                Color newCol1 = Tools.AddColors(initColor1, addNewCol);
                colors[modifTrInd] = newCol1;
            }
        }

        if (brushType == "SoftAdditive")
        {
            float[,] kernel = Tools.BlurKernel(blurBrushForce, (int)brushSize);
            int center = (int)brushSize;
            for (int i = 0; i < triangleIndexes.Count; i++)
            {
                int triangleIndex = triangleIndexes[i];
                float bigInd = Mathf.Max(triangleIndex, triangleIndexes[0]);
                float smallInd = Mathf.Min(triangleIndex, triangleIndexes[0]);
                int y = (int)(bigInd - smallInd + 2*(int)brushSize) /( 2 * numbVertPerSide);
                int x = Mathf.Abs((int)(bigInd - smallInd - 2*y*numbVertPerSide) / 2);
                
                float factor = kernel[center + x, center + y];
                Color addNewCol = new Color(-coloredSpeed * Time.deltaTime * factor, -coloredSpeed * Time.deltaTime * factor, -coloredSpeed * Time.deltaTime * factor);
                for (int j = 0; j < 3; j++)
                {
                    
                    Color initColor1 = colors[triangles[Mathf.Clamp(triangleIndex * 3 + j, 0, triangles.Length - 1)]];
                    Color newCol1 = Tools.AddColors(initColor1, addNewCol);
                    colors[triangles[Mathf.Clamp(triangleIndex * 3 + j, 0, triangles.Length - 1)]] = newCol1;
                    if (triangleIndex % 2 == 0)
                    {
                        Color initColor2 = colors[triangles[Mathf.Clamp((triangleIndex + 1) * 3 + j, 0, triangles.Length - 1)]];
                        Color newCol2 = Tools.AddColors(initColor2, addNewCol);
                        colors[triangles[Mathf.Clamp((triangleIndex + 1) * 3 + j, 0, triangles.Length - 1)]] = newCol2;
                    }
                    else
                    {
                        Color initColor2 = colors[triangles[Mathf.Clamp((triangleIndex - 1) * 3 + j, 0, triangles.Length - 1)]];
                        Color newCol2 = Tools.AddColors(initColor2, addNewCol);
                        colors[triangles[Mathf.Clamp((triangleIndex - 1) * 3 + j, 0, triangles.Length - 1)]] = newCol2;
                    }

                }
            }

        }
        if (brushType == "SoftSoustractive")
        {
            float[,] kernel = Tools.BlurKernel(blurBrushForce, (int)brushSize);
            Tools.Print2DArray(kernel);
            int center = (int)brushSize;
            for (int i = 0; i < triangleIndexes.Count; i++)
            {
                int triangleIndex = triangleIndexes[i];
                float bigInd = Mathf.Max(triangleIndex, triangleIndexes[0]);
                float smallInd = Mathf.Min(triangleIndex, triangleIndexes[0]);
                int y = (int)(bigInd - smallInd + 2 * (int)brushSize) / (2 * numbVertPerSide);
                int x = Mathf.Abs((int)(bigInd - smallInd - 2 * y * numbVertPerSide) / 2);

                float factor = kernel[center + x, center + y];
                print((x, y, factor));
                Color addNewCol = new Color(coloredSpeed * Time.deltaTime * factor, coloredSpeed * Time.deltaTime * factor, coloredSpeed * Time.deltaTime * factor);
                for (int j = 0; j < 3; j++)
                {

                    Color initColor1 = colors[triangles[Mathf.Clamp(triangleIndex * 3 + j, 0, triangles.Length - 1)]];
                    Color newCol1 = Tools.AddColors(initColor1, addNewCol);
                    colors[triangles[Mathf.Clamp(triangleIndex * 3 + j, 0, triangles.Length - 1)]] = newCol1;
                    if (triangleIndex % 2 == 0)
                    {
                        Color initColor2 = colors[triangles[Mathf.Clamp((triangleIndex + 1) * 3 + j, 0, triangles.Length - 1)]];
                        Color newCol2 = Tools.AddColors(initColor2, addNewCol);
                        colors[triangles[Mathf.Clamp((triangleIndex + 1) * 3 + j, 0, triangles.Length - 1)]] = newCol2;
                    }
                    else
                    {
                        Color initColor2 = colors[triangles[Mathf.Clamp((triangleIndex - 1) * 3 + j, 0, triangles.Length - 1)]];
                        Color newCol2 = Tools.AddColors(initColor2, addNewCol);
                        colors[triangles[Mathf.Clamp((triangleIndex - 1) * 3 + j, 0, triangles.Length - 1)]] = newCol2;
                    }

                }
            }

        }

        Mesh paperMesh = MeshGenerator.instance.UpdateMesh(mesh, vertices, triangles, colors);

        hit.collider.GetComponent<MeshFilter>().mesh = paperMesh;
        meshCollider.sharedMesh = paperMesh;


        //Mesh mapMesh = map.GetComponent<MeshFilter>().mesh;

        //float time1 = Time.realtimeSinceStartup;
        //mapMesh = MeshGenerator.instance.UpdateMesh(mapMesh, mapMesh.vertices, triangles, colors);

        
        
        ////print(Time.realtimeSinceStartup - time1);

        ////float time1 = Time.realtimeSinceStartup;
        //Mesh newMapMesh = MeshGenerator.instance.UpdateHeight(mapMesh, modifiedTrianglesIndices);
        ////print(Time.realtimeSinceStartup - time1);
        //map.GetComponent<MeshFilter>().mesh = newMapMesh;
        //map.GetComponent<MeshCollider>().sharedMesh = newMapMesh;

        //Debug.Log("hit.triangleIndex = " + triInd);
    }

    public void Draw2()
    {
        RaycastHit[] hits = Physics.SphereCastAll(cam.ScreenPointToRay(Input.mousePosition), brushSize);
        // Iterate through the hit objects
        foreach (RaycastHit hit in hits)
        {
            bool isOverUI = UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject();
            if(!isOverUI)
            {
                Vector3 mouse = Input.mousePosition;
                mouse.z = cam.transform.position.y;
                Vector3 point = cam.ScreenToWorldPoint(mouse);
                MeshCollider meshCollider = hit.collider as MeshCollider;
                Mesh mesh = meshCollider.sharedMesh;
                Vector3[] vertices = mesh.vertices;
                Color[] colors = mesh.colors;
                int[] triangles = mesh.triangles;
                List<int> modifiedTrianglesIndices = new List<int>();
                for (int i = 0; i < triangles.Length; i += 3)
                {
                    // Get the vertices of the triangle
                    Vector3 vertex1 = vertices[triangles[i]] + hit.collider.transform.position;
                    Vector3 vertex2 = vertices[triangles[i + 1]] + hit.collider.transform.position;
                    Vector3 vertex3 = vertices[triangles[i + 2]] + hit.collider.transform.position;

                    // Optionally, you can check if the triangle vertices are inside the sphere cast
                    bool triangleInsideSphereCast = IsVertexInsideSphereCast(vertex1, point) ||
                                                     IsVertexInsideSphereCast(vertex2, point) ||
                                                     IsVertexInsideSphereCast(vertex3, point);

                    if (triangleInsideSphereCast)
                    {
                        if (!modifiedTrianglesIndices.Contains(triangles[i]))
                        {
                            modifiedTrianglesIndices.Add(triangles[i]);
                        }
                        if (!modifiedTrianglesIndices.Contains(triangles[i + 1]))
                        {
                            modifiedTrianglesIndices.Add(triangles[i + 1]); ;
                        }
                        if (!modifiedTrianglesIndices.Contains(triangles[i + 2]))
                        {
                            modifiedTrianglesIndices.Add(triangles[i + 2]); ;
                        }

                        // Do something with the triangle (e.g., store it, draw it, etc.)
                        Debug.DrawLine(vertex1, vertex2, Color.red);
                        Debug.DrawLine(vertex2, vertex3, Color.red);
                        Debug.DrawLine(vertex3, vertex1, Color.red);
                    }
                }

                Color color = brushColor;
                brushTypes bt = (brushTypes)brushDropdown.value;
                brushType = bt.ToString();

                foreach (int modifTrInd in modifiedTrianglesIndices)
                {
                    if (brushType == "Solid")
                    {
                        Color newCol = color;
                        colors[modifTrInd] = newCol;
                    }
                    if (brushType == "SolidAdditive")
                    {
                        Color addNewCol = new Color(-coloredSpeed * Time.deltaTime, -coloredSpeed * Time.deltaTime, -coloredSpeed * Time.deltaTime);
                        Color initColor1 = colors[modifTrInd];
                        Color newCol1 = Tools.AddColors(initColor1, addNewCol);
                        colors[modifTrInd] = newCol1;
                    }
                    if (brushType == "SolidSoustractive")
                    {
                        Color addNewCol = new Color(coloredSpeed * Time.deltaTime, coloredSpeed * Time.deltaTime, coloredSpeed * Time.deltaTime);
                        Color initColor1 = colors[modifTrInd];
                        Color newCol1 = Tools.AddColors(initColor1, addNewCol);
                        colors[modifTrInd] = newCol1;
                    }
                    if (brushType == "SoftAdditive")
                    {
                        Vector3 vertex = mesh.vertices[modifTrInd] + hit.collider.transform.position;
                        Vector3 directionToVertex = vertex - point;
                        float dist = directionToVertex.magnitude;
                        float factor = (10 / (Mathf.PI * 2 * blurBrushForce * blurBrushForce)) * Mathf.Exp(-(dist * dist) / (2 * blurBrushForce * blurBrushForce));
                        Color addNewCol = new Color(-coloredSpeed * Time.deltaTime * factor, -coloredSpeed * Time.deltaTime * factor, -coloredSpeed * Time.deltaTime * factor);
                        Color initColor1 = colors[modifTrInd];
                        Color newCol1 = Tools.AddColors(initColor1, addNewCol);
                        colors[modifTrInd] = newCol1;
                    }
                    if (brushType == "SoftSoustractive")
                    {
                        Vector3 vertex = mesh.vertices[modifTrInd] + hit.collider.transform.position;
                        Vector3 directionToVertex = vertex - point;
                        float dist = directionToVertex.magnitude;
                        float factor = (10 / (Mathf.PI * 2 * blurBrushForce * blurBrushForce)) * Mathf.Exp(-(dist * dist) / (2 * blurBrushForce * blurBrushForce));
                        Color addNewCol = new Color(coloredSpeed * Time.deltaTime * factor, coloredSpeed * Time.deltaTime * factor, coloredSpeed * Time.deltaTime * factor);
                        Color initColor1 = colors[modifTrInd];
                        Color newCol1 = Tools.AddColors(initColor1, addNewCol);
                        colors[modifTrInd] = newCol1;
                    }
                }

                Mesh paperMesh = MeshGenerator.instance.UpdateMesh(mesh, vertices, triangles, colors);
                hit.collider.GetComponent<MeshFilter>().mesh = paperMesh;
                meshCollider.sharedMesh = paperMesh;
            }
        }   
    }

    bool IsVertexInsideSphereCast(Vector3 vertex, Vector3 point)
    {
        // Check if the vertex is inside the sphere cast
        Vector3 directionToVertex = vertex - point;
        float distanceToVertex = directionToVertex.magnitude;
        return distanceToVertex <= brushSize;
    }
}
public enum brushTypes
{
    Solid, SolidAdditive,SolidSoustractive, SoftAdditive, SoftSoustractive
}
