using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreviewCamera : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        transform.position = new Vector3(
            -1000 + MeshGenerator.instance.xSize * MeshGenerator.instance.numberOfInstances/2,
            MeshGenerator.instance.xSize * MeshGenerator.instance.numberOfInstances,
            -MeshGenerator.instance.xSize * MeshGenerator.instance.numberOfInstances / 2

            );
    }

   
}
