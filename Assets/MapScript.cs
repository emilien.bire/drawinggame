using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.TerrainTools;

public class MapScript : MonoBehaviour
{
    public GameObject paper;
    public GameObject water;
    public Vector3 mapOrigin;
    public float waterLevel;
    public Material terrainMaterial;
    public Material waterMaterial;
    private Mesh paperMesh;
    private Mesh mapMesh;


    private Vector3[] vertices;
    void Start()
    {
        //MeshGenerator.instance.CreateWholeMap(0f, MeshGenerator.instance.xSize, MeshGenerator.instance.zSize);
        Mesh waterMesh = water.GetComponent<MeshFilter>().mesh;
        water.transform.localPosition = new Vector3 (0, -2f,0);
    }

    public void UpdateMapMesh()
    {
        paperMesh = paper.GetComponent<MeshFilter>().mesh;
        mapMesh = GetComponent<MeshFilter>().mesh;
        GetComponent<MeshFilter>().mesh = MeshGenerator.instance.UpdateMesh(mapMesh, mapMesh.vertices, paperMesh.triangles, paperMesh.colors);
        Mesh newMesh = MeshGenerator.instance.UpdateHeight(mapMesh, Enumerable.Range(0, mapMesh.vertices.Length).ToList());
        GetComponent<MeshFilter>().mesh = newMesh;
        GetComponent<MeshCollider>().sharedMesh = newMesh;
        MakeWaterMesh(newMesh);

    }

    public void SmoothMapMesh()
    {
        paperMesh = paper.GetComponent<MeshFilter>().mesh;
        Mesh newMesh = MeshGenerator.instance.SmoothMesh(paperMesh);
        paper.GetComponent<MeshFilter>().mesh = newMesh;
        paper.GetComponent<MeshCollider>().sharedMesh = newMesh;
        UpdateMapMesh();
    }

    public void SharpenMapMesh()
    {
        paperMesh = paper.GetComponent<MeshFilter>().mesh;
        Mesh newMesh = MeshGenerator.instance.SharpenMesh(paperMesh);
        paper.GetComponent<MeshFilter>().mesh = newMesh;
        paper.GetComponent<MeshCollider>().sharedMesh = newMesh;
        UpdateMapMesh();
    }

    public void NoisedMapMesh()
    {
        paperMesh = paper.GetComponent<MeshFilter>().mesh;
        Mesh newMesh = MeshGenerator.instance.NoiseMesh(this.gameObject, paperMesh);
        paper.GetComponent<MeshFilter>().mesh = newMesh;
        paper.GetComponent<MeshCollider>().sharedMesh = newMesh;
        UpdateMapMesh();
    }

    void MakeWaterMesh(Mesh mesh)
    {
        int[] triangles = mesh.triangles;
        Vector3[] vertices = mesh.vertices;
        List<Vector3> waterVertices = new List<Vector3>();
        List<int> newTrianglesList = new List<int>();
        for (int i=0;i<vertices.Length;i++)
        {
            Vector3 vertex1 = vertices[i];
            
            if(vertex1.y <= waterLevel + 0.3)
            {
                waterVertices.Add(vertex1);
            }  
        }

        
        if(waterVertices.Count > 0)
        {
            float minX = waterVertices[0].x;
            float minZ = waterVertices[0].z;

            float maxX = minX;
            float maxZ = minZ;
            for (int i = 0; i < waterVertices.Count; i++)
            {
                if (waterVertices[i].x < minX)
                {
                    minX = waterVertices[i].x;
                }
                if (waterVertices[i].z < minZ)
                {
                    minZ = waterVertices[i].z;
                }

                if (waterVertices[i].x > maxX)
                {
                    maxX = waterVertices[i].x;
                }
                if (waterVertices[i].z > maxZ)
                {
                    maxZ = waterVertices[i].z;
                }
            }
            Mesh waterMesh = MeshGenerator.instance.CreateMesh(new Vector3(minX,0,minZ),0, (int)(maxX - minX), (int)(maxZ - minZ));
            waterMesh.RecalculateNormals();
            waterMesh.RecalculateTangents();
            water.GetComponent<MeshFilter>().mesh = waterMesh;
        }
        else
        {
            Mesh waterMesh = new Mesh();
            waterMesh.Clear();
            water.GetComponent<MeshFilter>().mesh = waterMesh;
        }
        

    }

}
