using UnityEngine;
using UnityEngine.UI;

public class SwitchEditorMapScript : MonoBehaviour
{
    public bool isInEditor = true;

    public Camera EditorCamera;
    public GameObject PlayerCamera;
    public GameObject Player;
    public GameObject[] CanvasObjects;

    public Button switchButton;

    public void OnSwitch()
    {
        if (isInEditor)
        {
            EditorCamera.enabled = false;
            PlayerCamera.SetActive(true);
            Player.SetActive(true);
            Player.GetComponent<PlayerMovement>().enabled = true;
            foreach (GameObject obj in CanvasObjects)
            {
                obj.SetActive(false);
            }
            isInEditor = false;
        }
        else
        {
            EditorCamera.enabled = true;
            PlayerCamera.SetActive(false);
            Player.SetActive(false);
            Player.GetComponent<PlayerMovement>().enabled = false;
            foreach (GameObject obj in CanvasObjects)
            {
                obj.SetActive(true);
            }
            isInEditor = true;
        }
    }
}
