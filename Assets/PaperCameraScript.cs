using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaperCameraScript : MonoBehaviour
{
    public float zoomSpeed;
    public float displacementSpeed;
    private Vector3 originalMousePoint;
    private Vector3 originalMapPosition;
    void Start()
    {
        transform.position = new Vector3(
            MeshGenerator.instance.numberOfInstances * MeshGenerator.instance.xSize/2,
            Mathf.Max(MeshGenerator.instance.numberOfInstances * MeshGenerator.instance.xSize, MeshGenerator.instance.numberOfInstances * MeshGenerator.instance.zSize),
            MeshGenerator.instance.numberOfInstances * MeshGenerator.instance.zSize/2);
    }

    private void Update()
    {
        Zoom(Input.mouseScrollDelta.y * MeshGenerator.instance.numberOfInstances * MeshGenerator.instance.xSize);

        if(Input.GetMouseButtonDown(1))
        {
            Vector3 mouse = Input.mousePosition;
            mouse.z = transform.position.y;
            originalMousePoint = GetComponent<Camera>().ScreenToWorldPoint(mouse);
            originalMapPosition = transform.position;
        }
        if (Input.GetMouseButton(1))
        {
            Vector3 mouse = Input.mousePosition;
            mouse.z = transform.position.y;
            Vector3 displacement = GetComponent<Camera>().ScreenToWorldPoint(mouse) - originalMousePoint;
            transform.position = originalMapPosition - displacement * displacementSpeed * MeshGenerator.instance.numberOfInstances * MeshGenerator.instance.xSize;
        }
    }

    void Zoom(float zoom)
    {
        transform.position = new Vector3 (transform.position.x, transform.position.y-Time.deltaTime*zoom*zoomSpeed, transform.position.z);
    }

}
