using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public GameObject map;

    [Header("Movement")]
    public float movementSpeed;
    public float groundDrag;
    public float airFriction;

    [Header("Ground check")]
    public float playerHeight;
    public LayerMask whatIsGround;
    bool grounded;

    public Transform orientation;

    float horizontalInput;
    float verticalInput;

    Vector3 moveDirection;
    

    private Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.freezeRotation = true;
    }
    void OnEnable()
    {
        transform.position = map.GetComponent<WholeMap>().spawnPoint.transform.position;
    }

    

    // Update is called once per frame
    void Update()
    {
        grounded = Physics.Raycast(transform.position, Vector3.down, playerHeight * 0.5f + 0.2f, whatIsGround);

        MyInput();
        SpeedControl();
        if (grounded)
        {
            rb.drag = groundDrag;
        }
        else
        {
            rb.drag = 0f;
        }
    }

    private void FixedUpdate()
    {
        MovePlayer();
        if (!grounded)
        {
            rb.AddForce(-rb.velocity * airFriction, ForceMode.Force);
        }

    }

    void MovePlayer()
    {
        moveDirection = orientation.forward*verticalInput + orientation.right * horizontalInput;
        //Vector3 dir = 
        //transform.Translate(Vector3.forward * Time.deltaTime * movementSpeed * z + Vector3.right * Time.deltaTime * movementSpeed * x);
        rb.AddForce(moveDirection * movementSpeed * 10f, ForceMode.Force);
    }

    private void MyInput()
    {
        horizontalInput = Input.GetAxis("Horizontal");
        verticalInput = Input.GetAxis("Vertical");
    }

    void SpeedControl()
    {
        Vector3 flatVel = new Vector3(rb.velocity.x, 0f, rb.velocity.z);

        if(flatVel.magnitude > movementSpeed)
        {
            Vector3 limitedVel = flatVel.normalized * movementSpeed;
            rb.velocity = new Vector3(limitedVel.x, rb.velocity.y, limitedVel.z);
        }
       
    }
}
