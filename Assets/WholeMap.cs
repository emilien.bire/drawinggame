using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class WholeMap : MonoBehaviour
{
    public GameObject spawnPoint;
    public Vector3 spawnPosition;
    public void UpdateWholeMap()
    {
        spawnPoint.transform.localPosition = spawnPosition;
        MapScript[] mapScripts = GetComponentsInChildren<MapScript>();
        foreach (MapScript mapScript in mapScripts)
        {
            mapScript.UpdateMapMesh();
        }
    }

    public void SmoothWholeMap()
    {
        
        MapScript[] mapScripts = GetComponentsInChildren<MapScript>();

        int numberOfLinesPerMesh = MeshGenerator.instance.zSize + 1;
        int numberOfColsPerMesh = MeshGenerator.instance.xSize + 1;
        int numberOfInstances = MeshGenerator.instance.numberOfInstances;

        List<float> cols = new List<float>();
        
        for (int i = 0; i < numberOfColsPerMesh * numberOfInstances; i++)
        {
            for (int k=0;k<numberOfInstances;k++)
            {
                Mesh paperBlockMeshes = mapScripts[k + (int)(i/ numberOfColsPerMesh) * numberOfInstances].paper.GetComponent<MeshFilter>().mesh;
                mapScripts[k  + (int)(i / numberOfColsPerMesh) * numberOfInstances].paper.name = (k + (int)(i/ numberOfColsPerMesh) * numberOfInstances).ToString();
                for (int j = 0; j < numberOfLinesPerMesh; j++)
                {
                    //cols.Add((float)((i % numberOfColsPerMesh) * numberOfColsPerMesh + j)/(numberOfColsPerMesh* numberOfLinesPerMesh));
                    if(i==0 && j==0 && k==0)
                    {
                        cols.Add(paperBlockMeshes.colors[(i % numberOfColsPerMesh) * numberOfColsPerMesh + j].grayscale);
                    }
                    
                    if ((i==0 && j!=0) || ( j == 0 && (k % numberOfInstances == 0 && i% numberOfColsPerMesh != 0)))
                    {
                        cols.Add(paperBlockMeshes.colors[(i % numberOfColsPerMesh) * numberOfColsPerMesh + j].grayscale);
                        
                    }

                    else if (i % numberOfColsPerMesh != 0 && j!=0)
                    {
                        cols.Add(paperBlockMeshes.colors[(i % numberOfColsPerMesh) * numberOfColsPerMesh + j].grayscale);
                    }
                }
            }
        }
        float[,] cols2D = Tools.ArrayTo2DArray(cols.ToArray(), (numberOfLinesPerMesh-1) * numberOfInstances +1, (numberOfColsPerMesh - 1) * numberOfInstances+1);
        float[,] newColors = MeshGenerator.instance.SmoothMeshColor(cols2D);
        for (int k = 0; k < numberOfInstances; k++)
        {
            for (int l = 0; l < numberOfInstances; l++)
            {
                Mesh paperMesh = mapScripts[l + k * numberOfInstances].paper.GetComponent<MeshFilter>().mesh;

                float[,] blockColors = new float[numberOfLinesPerMesh, numberOfColsPerMesh];
                for (int i = 0; i < numberOfColsPerMesh; i++)
                {
                    for (int j = 0; j < numberOfLinesPerMesh; j++) 
                    {
                        blockColors[j,i] = newColors[j + l*(numberOfLinesPerMesh-1) ,i + k*(numberOfColsPerMesh-1)];
                    }
                }
                
                float[] newMeshCols = Tools.flatten(blockColors);
                //Tools.Print2DArray(Tools.ArrayTo2DArray(newMeshCols, numberOfLinesPerMesh, numberOfColsPerMesh));
                paperMesh = MeshGenerator.instance.UpdateMesh(paperMesh, paperMesh.vertices, paperMesh.triangles, Tools.GreyScaleToRGB(newMeshCols));
                mapScripts[l + k * numberOfInstances].paper.GetComponent<MeshFilter>().mesh = paperMesh;
                mapScripts[l + k * numberOfInstances].paper.GetComponent<MeshCollider>().sharedMesh = paperMesh;
            }
        }

        UpdateWholeMap();

    }

    public void SharpenWholeMap()
    {

        MapScript[] mapScripts = GetComponentsInChildren<MapScript>();

        int numberOfLinesPerMesh = MeshGenerator.instance.zSize + 1;
        int numberOfColsPerMesh = MeshGenerator.instance.xSize + 1;
        int numberOfInstances = MeshGenerator.instance.numberOfInstances;

        List<float> cols = new List<float>();

        for (int i = 0; i < numberOfColsPerMesh * numberOfInstances; i++)
        {
            for (int k = 0; k < numberOfInstances; k++)
            {
                Mesh paperBlockMeshes = mapScripts[k + (int)(i / numberOfColsPerMesh) * numberOfInstances].paper.GetComponent<MeshFilter>().mesh;
                mapScripts[k + (int)(i / numberOfColsPerMesh) * numberOfInstances].paper.name = (k + (int)(i / numberOfColsPerMesh) * numberOfInstances).ToString();
                for (int j = 0; j < numberOfLinesPerMesh; j++)
                {
                    //cols.Add((float)((i % numberOfColsPerMesh) * numberOfColsPerMesh + j)/(numberOfColsPerMesh* numberOfLinesPerMesh));
                    if (i == 0 && j == 0 && k == 0)
                    {
                        cols.Add(paperBlockMeshes.colors[(i % numberOfColsPerMesh) * numberOfColsPerMesh + j].grayscale);
                    }

                    if ((i == 0 && j != 0) || (j == 0 && (k % numberOfInstances == 0 && i % numberOfColsPerMesh != 0)))
                    {
                        cols.Add(paperBlockMeshes.colors[(i % numberOfColsPerMesh) * numberOfColsPerMesh + j].grayscale);

                    }

                    else if (i % numberOfColsPerMesh != 0 && j != 0)
                    {
                        cols.Add(paperBlockMeshes.colors[(i % numberOfColsPerMesh) * numberOfColsPerMesh + j].grayscale);
                    }

                }
            }
        }
        float[,] cols2D = Tools.ArrayTo2DArray(cols.ToArray(), (numberOfLinesPerMesh - 1) * numberOfInstances + 1, (numberOfColsPerMesh - 1) * numberOfInstances + 1);
        float[,] newColors = MeshGenerator.instance.SharpenMeshColor(cols2D);
        for (int k = 0; k < numberOfInstances; k++)
        {
            for (int l = 0; l < numberOfInstances; l++)
            {
                Mesh paperMesh = mapScripts[l + k * numberOfInstances].paper.GetComponent<MeshFilter>().mesh;

                float[,] blockColors = new float[numberOfLinesPerMesh, numberOfColsPerMesh];
                for (int i = 0; i < numberOfColsPerMesh; i++)
                {
                    for (int j = 0; j < numberOfLinesPerMesh; j++)
                    {
                        blockColors[j, i] = newColors[j + l * (numberOfLinesPerMesh - 1), i + k * (numberOfColsPerMesh - 1)];
                    }
                }

                float[] newMeshCols = Tools.flatten(blockColors);
                //Tools.Print2DArray(Tools.ArrayTo2DArray(newMeshCols, numberOfLinesPerMesh, numberOfColsPerMesh));
                paperMesh = MeshGenerator.instance.UpdateMesh(paperMesh, paperMesh.vertices, paperMesh.triangles, Tools.GreyScaleToRGB(newMeshCols));
                mapScripts[l + k * numberOfInstances].paper.GetComponent<MeshFilter>().mesh = paperMesh;
                mapScripts[l + k * numberOfInstances].paper.GetComponent<MeshCollider>().sharedMesh = paperMesh;
            }
        }

        UpdateWholeMap();

    }

    public void NoiseWholeMap()
    {
        MapScript[] mapScripts = GetComponentsInChildren<MapScript>();
        foreach (MapScript mapScript in mapScripts)
        {
            mapScript.NoisedMapMesh();
        }
    }
}
