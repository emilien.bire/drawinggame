using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UIElements;

public class MeshGenerator : MonoBehaviour
{
    public int xSize;
    public int zSize;
    public int numberOfInstances;
    //public int numberOfVertPerSide;
    public float maxHeight;
    public float smoothSigma = 1;
    public int smoothKernelSize;

    public int sharpeningStrength = 6;
    

    public float noiseOffsetX = 0f;
    public float noiseOffsetY = 0f;
    public float noiseStrength = 0.5f;
    public float noiseScale =1;

    public GameObject MapParent;
    public GameObject mapInstancePrefab;

    public GameObject PaperParent;
    public GameObject paperInstancePrefab;
    public static MeshGenerator instance;

    

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        CreateWholeMap(0f,xSize,zSize);
    }

    public void CreateWholeMap(float yOffset, int xSize, int zSize)
    {
         Mesh[,] meshes = new Mesh[numberOfInstances, numberOfInstances];
        for (int i = 0; i < numberOfInstances; i++)
        {
            for(int j = 0; j < numberOfInstances; j++)
            {
                Mesh mesh = CreateMesh(new Vector3(0, 0, 0), yOffset, xSize, zSize);
                meshes[i, j] = mesh;
                GameObject newMapBlock = Instantiate(mapInstancePrefab, new Vector3(i * xSize, 0, j * zSize) + MapParent.transform.position, Quaternion.identity, MapParent.transform);
                newMapBlock.GetComponent<MeshFilter>().mesh = mesh;
                newMapBlock.GetComponent<MeshCollider>().sharedMesh = mesh;

                GameObject newPaperBlock = Instantiate(paperInstancePrefab, new Vector3(i * xSize, 0, j * zSize) + PaperParent.transform.position, Quaternion.identity, PaperParent.transform);
                newPaperBlock.GetComponent<MeshFilter>().mesh = mesh;
                newPaperBlock.GetComponent<MeshCollider>().sharedMesh = mesh;

                newMapBlock.GetComponent<MapScript>().paper = newPaperBlock;

            }
        }
    }

    public Mesh CreateMesh(Vector3 origin, float yOffset, int xSize, int zSize)
    {
        Vector3[] vertices = new Vector3[(xSize + 1) * (zSize + 1)];
        int k = 0;
        for (int x = 0; x <= xSize; x++)
        { 
            for (int z = 0; z <= zSize; z++)
            {
                //float y = Mathf.PerlinNoise(x * (xSize / numberOfVertPerSide) * .3f, z * (zSize / numberOfVertPerSide) * .3f) * 2f;

                vertices[k] = new Vector3(origin.x + x,
                                            origin.y + yOffset,
                                            origin.z + z
                                            );
                k ++;
            }
        }

        int[] triangles = new int[6 * xSize * zSize];
        int vert = 0;
        int tris = 0;

        for (int x = 0; x < xSize; x++)
        {
            for (int z = 0; z < zSize; z++)
            {
                triangles[0 + tris] = vert + 1;
                triangles[1 + tris] = vert + zSize + 1;
                triangles[2 + tris] = vert + 0;
                triangles[3 + tris] = vert + zSize + 2;
                triangles[4 + tris] = vert + zSize + 1;
                triangles[5 + tris] = vert + 1;
                vert++;
                tris += 6;
            }
            vert++;
        }

        Color[] colors = new Color[vertices.Length];
        k = 0;
        IEnumerable<float> heights = from number in Enumerable.Range(0, vertices.Length) select vertices[number].y;
        for (int i = 0; i <= xSize; i++)
        {
            for (int j = 0; j <= zSize; j++)
            {
                float height = heights.ElementAt(k);
                //print((height - heights.Min()) / (heights.Max() - heights.Min()));
                colors[k] = new Color(0.5f, 0.5f, 0.5f);
                k++;
            }
        }
        Mesh mesh = new Mesh();


        return UpdateMesh(mesh, vertices, triangles, colors);

    }
    public Mesh UpdateMesh(Mesh mesh, Vector3[] vertices, int[] triangles, Color[] colors)
    {
        mesh.Clear();
        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.colors = colors;
        //GetComponent<MeshCollider>().sharedMesh = mesh;
        mesh.RecalculateNormals();
        return mesh;
    }

    public Mesh UpdateHeight(Mesh mesh, List<int> indexes)
    {
        Vector3[] newVert = mesh.vertices;
        foreach (int index in indexes)
        {
            float l = mesh.colors[index].grayscale;
            //float height = maxHeight * (1 - l) - maxHeight * l;
            float height = 0;
            if(l>0 && l<1)
            {
                height = Mathf.Clamp(Mathf.Log(l / (1 - l))*(maxHeight/6), -maxHeight, maxHeight);
            }
            if(l == 0)
            {
                height = -maxHeight;
            }
            if (l == 1)
            {
                height = maxHeight;
            }
            newVert[index] = new Vector3(newVert[index].x, -height, newVert[index].z);
        }
        return UpdateMesh(mesh, newVert, mesh.triangles, mesh.colors);
    }

    public Mesh SmoothMesh(Mesh mesh)
    {
        float[] cols = new float[mesh.colors.Length];
        for(int i=0;i<cols.Length;i++)
        {
            Color col = mesh.colors[i];
            cols[i] = col.grayscale;
        }

        float[,] cols2D = Tools.ArrayTo2DArray(cols, xSize +1, zSize + 1);
        float[,] kernel = Tools.BlurKernel(smoothSigma, smoothKernelSize);
        float[] newCols = Tools.flatten(Tools.Convolution(cols2D, kernel));

        return UpdateMesh(mesh, mesh.vertices, mesh.triangles, Tools.GreyScaleToRGB(newCols));
    }

    public float[,] SmoothMeshColor(float[,] cols)
    {
        
        
        float[,] kernel = Tools.BlurKernel(smoothSigma, smoothKernelSize);
        float[,] newCols = Tools.Convolution(cols, kernel);

        return newCols;
    }

    public Mesh SharpenMesh(Mesh mesh)
    {
        float[] cols = new float[mesh.colors.Length];
        for (int i = 0; i < cols.Length; i++)
        {
            Color col = mesh.colors[i];
            cols[i] = col.grayscale;
        }

        float[,] cols2D = Tools.ArrayTo2DArray(cols, xSize + 1, zSize + 1);
        float[,] kernel = Tools.SharpeningFilter(sharpeningStrength);
        float[] newCols = Tools.flatten(Tools.Convolution(cols2D, kernel));

        return UpdateMesh(mesh, mesh.vertices, mesh.triangles, Tools.GreyScaleToRGB(newCols));
    }

    public float[,] SharpenMeshColor(float[,] cols)
    {

        float[,] kernel = Tools.SharpeningFilter(sharpeningStrength);
        float[,] newCols = Tools.Convolution(cols, kernel);

        return newCols;
    }

    public Mesh NoiseMesh(GameObject mapBlock, Mesh mesh)
    {
        Vector3 mapBlockPos = mapBlock.transform.localPosition;
        Color[] colors = mesh.colors;
        for(int i=0;i< colors.Length;i++)
        {
            float colorGS = colors[i].grayscale;
            Vector3 vertex = mesh.vertices[i];
            float y = noiseStrength * (0.5f-Mathf.PerlinNoise((vertex.x + mapBlockPos.x)/ xSize * noiseScale + noiseOffsetX , (vertex.z + mapBlockPos.z)/ zSize * noiseScale + noiseOffsetY ));
            
            Color color = new Color(Mathf.Clamp(colorGS + y, 0,1), Mathf.Clamp(colorGS + y, 0, 1), Mathf.Clamp(colorGS + y, 0, 1));
            colors[i] = color;
        }
        

        return UpdateMesh(mesh, mesh.vertices, mesh.triangles, colors);
    }

}
